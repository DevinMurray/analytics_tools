﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics.Experimental;

public class GameManager : MonoBehaviour
{
    public static int CurrentLevelNumber;

    public static int levelTimelapse;
	public int numberOfClicks;

    [System.Flags]
    public enum Symbols
    {
        Waves =             1 << 0,
        Dot =               1 << 1,
        Square =            1 << 2,
        LargeDiamond =      1 << 3,
        SmallDiamonds =     1 << 4,
        Command =           1 << 5,
        Bomb =              1 << 6,
        Sun =               1 << 7,
        Bones =             1 << 8,
        Drop =              1 << 9,
        Face =              1 << 10,
        Hand =              1 << 11,
        Flag =              1 << 12,
        Disk =              1 << 13,
        Candle =            1 << 14,
        Wheel =             1 << 15
    }

    [System.Serializable]
    public struct LevelDescription
    {
        public int Rows, Columns;
        [EnumFlags]
        public Symbols UsedSymbols;
    }

    public GameObject TilePrefab;
    public float TileSpacing;

    public LevelDescription[] Levels = new LevelDescription[0];
    public Action HideTilesEvent;

    private Tile m_tileOne, m_tileTwo;
    private int m_cardsRemaining;
    
    private void Start()
    {
        LoadLevel( CurrentLevelNumber );
    }

    public void Update()
    {
        Debug.Log(CurrentLevelNumber + " " + Levels.Length);
    }

    //  Initializing the level using the rows, columns, and symbols assigned from the inspector
    private void LoadLevel( int levelNumber )
    {
        LevelDescription level = Levels[levelNumber % Levels.Length];

        List<Symbols> symbols = GetRequiredSymbols( level );

        //  Instantiating the tiles in place of rows and columns
        for ( int rowIndex = 0; rowIndex < level.Rows; ++rowIndex )
        {
            float yPosition = rowIndex * ( 1 + TileSpacing );
            for ( int colIndex = 0; colIndex < level.Columns; ++colIndex )
            {
                float xPosition = colIndex * ( 1 + TileSpacing );
                GameObject tileObject = Instantiate( TilePrefab, new Vector3( xPosition, yPosition, 0 ), Quaternion.identity, this.transform );
                int symbolIndex = UnityEngine.Random.Range( 0, symbols.Count );
                tileObject.GetComponentInChildren<Renderer>().material.mainTextureOffset = GetOffsetFromSymbol( symbols[symbolIndex] );
                tileObject.GetComponent<Tile>().Initialize( this, symbols[symbolIndex] );
                symbols.RemoveAt( symbolIndex );
            }
        }

        SetupCamera( level );
    }

    //  Declaring which symbols to use for the level
    private List<Symbols> GetRequiredSymbols( LevelDescription level )
    {
        List<Symbols> symbols = new List<Symbols>();
        int cardTotal = level.Rows * level.Columns;

        //  Restricting the total number of cards to an even number, creates a list of symbols to use in the level
        {
            Array allSymbols = Enum.GetValues( typeof( Symbols ) );

            m_cardsRemaining = cardTotal;

            if ( cardTotal % 2 > 0 )
            {
                new ArgumentException( "There must be an even number of cards" );
            }

            foreach ( Symbols symbol in allSymbols )
            {
                if ( ( level.UsedSymbols & symbol ) > 0 )
                {
                    symbols.Add( symbol );
                }
            }
        }

        //  Debug arguments to prevent the game from crashing if there are too many or too few cards
        {
            if ( symbols.Count == 0 )
            {
                new ArgumentException( "The level has no symbols set" );
            }
            if ( symbols.Count > cardTotal / 2 )
            {
                new ArgumentException( "There are too many symbols for the number of cards." );
            }
        }

        //  Prevents the random assortment of symbols from holding too many repeated symbols
        {
            int repeatCount = ( cardTotal / 2 ) - symbols.Count;
            if ( repeatCount > 0 )
            {
                List<Symbols> symbolsCopy = new List<Symbols>( symbols );
                List<Symbols> duplicateSymbols = new List<Symbols>();
                for ( int repeatIndex = 0; repeatIndex < repeatCount; ++repeatIndex )
                {
                    int randomIndex = UnityEngine.Random.Range( 0, symbolsCopy.Count );
                    duplicateSymbols.Add( symbolsCopy[randomIndex] );
                    symbolsCopy.RemoveAt( randomIndex );
                    if ( symbolsCopy.Count == 0 )
                    {
                        symbolsCopy.AddRange( symbols );
                    }
                }
                symbols.AddRange( duplicateSymbols );
            }
        }

        symbols.AddRange( symbols );

        return symbols;
    }

    //  Offsetting the symbol renderer, placing it on the surface of the card and not inside of it
    private Vector2 GetOffsetFromSymbol( Symbols symbol )
    {
        Array symbols = Enum.GetValues( typeof( Symbols ) );
        for ( int symbolIndex = 0; symbolIndex < symbols.Length; ++symbolIndex )
        {
            if ( ( Symbols )symbols.GetValue( symbolIndex ) == symbol )
            {
                return new Vector2( symbolIndex % 4, symbolIndex / 4 ) / 4f;
            }
        }
        Debug.Log( "Failed to find symbol" );
        return Vector2.zero;
    }

    //  Places the camera in the middle of the screen no matter how many rows or columns there are
    private void SetupCamera( LevelDescription level )
    {
        Camera.main.orthographicSize = ( level.Rows + ( level.Rows + 1 ) * TileSpacing ) / 2;
        Camera.main.transform.position = new Vector3( ( level.Columns * ( 1 + TileSpacing ) ) / 2, ( level.Rows * ( 1 + TileSpacing ) ) / 2, -10 );
    }

    //  Shows tiles on mouse click, displaying only two at a time. If both match match is true, else it's false
    public void TileSelected( Tile tile )
    {
		numberOfClicks++;

		Analytics.CustomEvent("TileClicked", new Dictionary <string, object>
			{
				{ "mouseClicks", numberOfClicks }
			});
		
        if ( m_tileOne == null )
        {
            m_tileOne = tile;
            m_tileOne.Reveal();
        }
        else if ( m_tileTwo == null )
        {
            m_tileTwo = tile;
            m_tileTwo.Reveal();
            if ( m_tileOne.Symbol == m_tileTwo.Symbol )
            {
                Analytics.CustomEvent("TilesMatch");

                StartCoroutine( WaitForHide( true, 1f ) );
            }
            else
            {
                Analytics.CustomEvent("TilesDoNotMatch");

                StartCoroutine( WaitForHide( false, 1f ) );
            }
        }
    }

    //  Changes level or displays game over on level complete
    private void LevelComplete()
    {
		AnalyticsEvent.LevelComplete (CurrentLevelNumber.ToString(), new Dictionary <string, object> () 
		{
			{ "timeElapsed", Time.timeSinceLevelLoad } 
		});

        levelTimelapse = Mathf.FloorToInt(Time.timeSinceLevelLoad);
		numberOfClicks = 0;

        Debug.Log(levelTimelapse);

        ++CurrentLevelNumber;
        if ( CurrentLevelNumber > Levels.Length - 1 )
        {
            Debug.Log( "GameOver" );
        }
        else
        {
            SceneManager.LoadScene( SceneManager.GetActiveScene().buildIndex );
        }
    }

    //  Hides or destroys active cards after a given time period. If 0 cards remain LeveLComplete is called
    private IEnumerator WaitForHide( bool match, float time )
    {
        float timer = 0;
        while ( timer < time )
        {
            timer += Time.deltaTime;
            if ( timer >= time )
            {
                break;
            }
            yield return null;
        }
        if ( match )
        {
            Destroy( m_tileOne.gameObject );
            Destroy( m_tileTwo.gameObject );
            m_cardsRemaining -= 2;
        }
        else
        {
            m_tileOne.Hide();
            m_tileTwo.Hide();
        }
        m_tileOne = null;
        m_tileTwo = null;

        if ( m_cardsRemaining == 0 )
        {
            LevelComplete();
        }
    }
}
